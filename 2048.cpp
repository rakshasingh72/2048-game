#include <bits/stdc++.h>
#include <iomanip>

using namespace std;

#define ROWS (4)
#define COLS (4)
#define WINNING_SCORE (2048)
class Game {
   private:
    vector<vector<int>> board;

    unordered_map<char, bool> movement;

    bool win = false;

   public:
    Game() {
        board.resize(ROWS);
        for (int i = 0; i < ROWS; i++) {
            board[i].resize(COLS);
        }
        cout << "Board size:" << board.size() << " " << board[0].size() << endl;
        generate_number();
        print_board();
        set_movements_true();
    }
    void set_movements_true() {
        movement = {{'i', 1}, {'j', 1}, {'k', 1}, {'l', 1}};
    }
    // pick a random row  and search empty cell going left to right
    void generate_number() {
        srand(time(0));
        int row = rand() % ROWS;
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLS; j++) {
                if (board[row][j] == 0) {
                    board[row][j] = 2;
                    break;
                }
            }
            row = (row + 1) % 4;
        }
    }

    void print_board() {
        cout << "--------------" << endl;
        cout << "--------------" << endl;
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLS; j++) {
                cout << board[i][j] << "  ";
            }
            cout << endl;
        }
    }
    bool get_win_status() { return win; }
    void set_movement(char input, bool moved) { movement[input] = moved; }
    bool get_movement(char input) { return movement[input]; }
    bool merge(vector<int> &row, int j, int i, string shift) {
        int n = ROWS - 1;
        bool move = false;
        while (n) {
            if (row[i] != 0) {
                if (row[j] == 0) {
                    row[j] = row[i];
                    row[i] = 0;
                    move = true;
                } else if (row[i] == row[j]) {
                    row[j] = 2 * row[j];
                    if (row[j] == WINNING_SCORE) {
                        win = true;
                    }
                    row[i] = 0;
                    if (shift == "left") {
                        j++;
                    } else {
                        j--;
                    }
                    move = true;
                } else {
                    if (shift == "left") {
                        j++;
                    } else {
                        j--;
                    }
                    row[j] = row[i];
                    if (j != i) {
                        row[i] = 0;
                        move = true;
                    }
                }
            }
            if (shift == "left") {
                i++;
            } else {
                i--;
            }
            n--;
        }
        return move;
    }
    void shift_right() {
        bool moved = false;
        for (int i = 0; i < ROWS; i++) {
            moved = merge(board[i], ROWS - 1, ROWS - 2, "right") or moved;
        }
        set_movement('l', moved);
    }
    void shift_left() {
        bool moved = false;
        for (int i = 0; i < ROWS; i++) {
            moved = merge(board[i], 0, 1, "left") or moved;
        }
        set_movement('j', moved);
    }
    void shift_down() {
        bool moved = false;
        for (int j = 0; j < COLS; j++) {
            vector<int> col_vec(ROWS);
            for (int i = 0; i < ROWS; i++) {
                col_vec[i] = board[i][j];
            }
            moved = merge(col_vec, ROWS - 1, ROWS - 2, "right") or moved;
            for (int i = 0; i < ROWS; i++) {
                board[i][j] = col_vec[i];
            }
        }
        set_movement('k', moved);
    }
    void shift_up() {
        bool moved = false;
        for (int j = 0; j < COLS; j++) {
            vector<int> col_vec(ROWS);
            for (int i = 0; i < ROWS; i++) {
                col_vec[i] = board[i][j];
            }
            moved = merge(col_vec, 0, 1, "left") or moved;
            for (int i = 0; i < ROWS; i++) {
                board[i][j] = col_vec[i];
            }
        }
        set_movement('i', moved);
    }
};

int main() {
    Game g;
    cout << "Press 'j':left, 'l':right, 'i':up, 'k': down" << endl;
    // bool left = 0, right = 0, up = 0, down = 0;
    char input;
    while (1) {
        cin >> input;
        switch (input) {
            case 'j': {
                g.shift_left();
                break;
            }
            case 'l': {
                g.shift_right();
                break;
            }
            case 'i': {
                g.shift_up();
                break;
            }
            case 'k': {
                g.shift_down();
                break;
            }
        }
        if (g.get_movement(input)) {
            g.generate_number();
            system("clear");
            g.print_board();
            g.set_movements_true();
        }
        if (g.get_win_status()) {
            cout << "You win" << endl;
            break;
        }
        if (!g.get_movement('j') and !g.get_movement('l') and
            !g.get_movement('i') and !g.get_movement('k')) {
            cout << "Game over" << endl;
            break;
        }
    }
}
